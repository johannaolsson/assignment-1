//          BANK

const bankMoneyElement = document.getElementById("bankMoney")
const outstandingBankMoneyElement = document.getElementById("outstandingBankMoney")
const visibilityElement = document.getElementById("visibility")
const loanButtonElement = document.getElementById("loanButton")
const repayLoanButtonElement = document.getElementById("repayLoanButton")

let loan = 0
let bankMoney = 0
visibilityElement.style.visibility = "hidden"
repayLoanButtonElement.style.visibility = "hidden"


// Get a loan Button

function getALoan() {
  if (loan != 0) {
    alert("You can't take more than one loan")
  } else {
    loan = parseInt(prompt("Enter an amount you want to loan:"))
  if (loan <= bankMoney * 2 ) {
    visibilityElement.style.visibility = "visible"
    repayLoanButtonElement.style.visibility = "visible"
    outstandingBankMoneyElement.innerText = loan
  } else  {
    alert("You can't take this loan")
    visibilityElement.style.visibility = "hidden"
    repayLoanButtonElement.style.visibility = "hidden"
  }
}
}

loanButtonElement.addEventListener("click", getALoan)



//          WORK

const workButtonElement = document.getElementById("workButton")
const myMoneyElement = document.getElementById("myMoney")
const bankButtonElement = document.getElementById("bankButton")

let myMoney = 0


// Bank Button

function transferMoney () {
    
  if (loan > 0) {
    loan += myMoney * 0.1
    outstandingBankMoneyElement.innerText = loan
    bankMoney += myMoney * 0.9
    myMoney = 0
    myMoneyElement.innerText = myMoney
    bankMoneyElement.innerText = bankMoney
   }

  else if (loan === 0)
  bankMoney += myMoney
  myMoney = 0
  myMoneyElement.innerText = myMoney
  bankMoneyElement.innerText = bankMoney
   }

bankButtonElement.addEventListener("click", transferMoney)


// Work Button

function increaseMoney() {
  myMoney += 100
  myMoneyElement.innerText = myMoney
  }
  
  workButtonElement.addEventListener("click", increaseMoney)



// Repay Loan Button

function repayLoan () {
    
  if (myMoney === loan) {
  loan -= myMoney
  myMoney = 0
  myMoneyElement.innerText = myMoney
  outstandingBankMoneyElement.innerText = loan
  }

   else if (myMoney > loan) {
   myMoney -= loan
   loan = 0
   bankMoney += myMoney
   myMoney = 0
   myMoneyElement.innerText = myMoney
   outstandingBankMoneyElement.innerText = loan
   bankMoneyElement.innerText = bankMoney
   }

}

repayLoanButtonElement.addEventListener("click", repayLoan)



  //          LAPTOPS

const laptopsElement = document.getElementById("laptops")
const specsElement = document.getElementById("specs")
let priceElement = document.getElementById("price")
const titleElement = document.getElementById("laptopTitle")
const infoElement = document.getElementById("info")
const imageElement = document.getElementById("image")

let laptops = []
let specs = []
let price = 0
let laptopTitle = []
let info = []
let image = []


// Fetch from API

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x))
    specsElement.innerText = laptops[0].specs
    priceElement.innerText = laptops[0].price
    titleElement.innerText = laptops[0].title
    infoElement.innerText = laptops[0].description
    imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image
    price = laptops[0].price
}

const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    laptopsElement.appendChild(laptopElement)
}

const handleLaptopMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex]
    specsElement.innerText = selectedLaptop.specs
    priceElement.innerText = selectedLaptop.price
    titleElement.innerText = selectedLaptop.title
    infoElement.innerText = selectedLaptop.description
    imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image
    price = selectedLaptop.price 
}


laptopsElement.addEventListener("change", handleLaptopMenuChange)


// Buy Know Button

function buyLaptop () {
  if (price <= bankMoney) {
    bankMoney = bankMoney - price
    bankMoneyElement.innerText = bankMoney
    alert("You are know the owner of the laptop!")
  }

 else  {
  alert("Sorry, you cannot afford the laptop")
 }
}